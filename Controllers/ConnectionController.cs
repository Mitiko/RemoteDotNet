using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Rimotr.ExtensionMethods;
using Rimotr;

namespace Rimotr.Controllers
{
    [Route(Globals.apiRoute)]
    public class ConnectionsController : Controller
    {
        private static ConnectionsContext db = Globals.db;

        // POST api/v0/connections/get {"userId": "1246-4853-784531976-451236789412", "entityName": "school", computerName": "c7"}
        [HttpPost("connection/get")]
        public IActionResult GetLast([FromBody] string userId, [FromBody] string entityName, [FromBody] string computerName)
        {
            var user = db.Users
                .Find(new User(userId));

            var computer = user?.Computers
                .Where(c => c.Name == computerName);
            
            if(computer is null)
            {
                return BadRequest(new {message = "Computer id is wrong or user id is wrong"});
            }

            var log = db.Logs
                .Where(lg => lg.User == user && lg.Computer == computer)
                .OrderByDescending(lg => lg.Time)
                .FirstOrDefault();
                
            return Json(log);
        }

        // POST api/v0/connections/get {"userId": "1246-4853-784531976-451236789412", "entityName": "school", computerName": "c7"}
        [HttpPost("connection/get")]
        public IActionResult GetAll([FromBody] string userId, [FromBody] string entityName)
        {
            var user = db.Users
                .Find(new User(userId));

            var logs = db.Logs
                .Where(lg => lg.User == user && user.Computers.Contains(lg.Computer))
                .OrderByDescending(lg => lg.Time)
                .AsEnumerable();

            return Json(logs);
        }

        // POST api/v0/connections/post {"userId": "1246-4853-784531976-451236789412", "message": {"Command": "git", "Parameters": "push", "EntityName": "school", "ComputerName": "c7", "SenderIsUser": false}}
        [HttpPost("connections/post")]
        public IActionResult PostNew([FromBody] string computerId, [FromBody] Message msg)
        {
            var computer = db.Computers.Find(new Computer(computerId));
            var user = computer?.Owner;
            var log = new ConnectionLog { 
                Computer = computer, 
                User = user, 
                Message = msg, 
                Time = DateTime.Now};

            db.Logs.Add(log);
            db.SaveChanges();

            return Ok();
        }
    }
}