using System;

namespace Rimotr
{
    public class Message
    {
        public Guid Id { get; set; }
        public string Command { get; set; }
        public string Parameters { get; set; }
        public string EntityName { get; set; }
        public string ComputerName { get; set; }
        public bool SenderIsUser { get; set; }
    }
}