using System;
using System.Collections.Generic;

namespace Rimotr
{
    public class ConnectionLog
    {
        public Guid Id { get; set; }
        public Computer Computer { get; set; }
        public User User { get; set; }
        public Message Message { get; set; }
        public DateTime Time { get; set; }
    }
}