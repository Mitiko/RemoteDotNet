using System;
using System.Collections.Generic;
using System.Linq;

namespace Rimotr
{
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Role { get; set; } = "user";
        public IEnumerable<Computer> Computers { get; set; }

        public User(string id)
        {
            try
            {
                this.Id = Guid.Parse(id);
            }
            catch (ArgumentNullException)
            {
                throw;
            }
        }

        public User(string name, string password)
        {
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
            this.Password = password ?? throw new ArgumentNullException(nameof(password));
        }
    }
}