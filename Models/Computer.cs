using System;

namespace Rimotr
{
    public class Computer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public User Owner { get; set; }

        public Computer(string id)
        {
            this.Id = Guid.Parse(id);
        }
    }
}