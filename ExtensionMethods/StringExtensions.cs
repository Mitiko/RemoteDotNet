using System.Security.Cryptography;
using static System.Text.Encoding;

namespace Rimotr.ExtensionMethods
{
    public static class StringExtensions
    {
        public static string Hash(this string password)
        {
            var md5 = new MD5CryptoServiceProvider();
            var data = md5.ComputeHash(ASCII.GetBytes(password));
            var hashedPassword = ASCII.GetString(data);

            return hashedPassword;
        }
    }
}